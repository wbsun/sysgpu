DOC 		:= paper
EXTRAFIGPDF	:= $(wildcard figs/*.pdf)
PDFLATEX	:= env TEXINPUTS=.:: pdflatex
BIBTEX		:= env BIBINPUTS=.:: bibtex

PDFLATEXFLAGS	:= --interaction=nonstopmode --halt-on-error --file-line-error

TEXBITS		:= $(wildcard *.tex)
BIBBITS		:= $(wildcard *.bib)

FIGBITS		:= $(wildcard figs/*.fig)
FIGPDF		:= $(FIGBITS:.fig=.pdf) $(EXTRAFIGPDF)

DEPS		:= $(wildcard *.cls) $(wildcard *.sty) $(wildcard *.cfg) \
		   $(TEXBITS) $(BIBBITS) \
		   Makefile


.PHONY: all clean realclean
all::
clean::
realclean::


all:: $(DOC)

.PHONY: $(DOC)
$(DOC): $(DOC).pdf

$(addsuffix .pdf,$(DOC)): $(DEPS) $(FIGPDF)

%.pdf: %.tex
	$(PDFLATEX) $(PDFLATEXFLAGS) $(basename $<)
	- $(BIBTEX) $(basename $@)
	$(PDFLATEX) $(PDFLATEXFLAGS) $(basename $<)
	$(PDFLATEX) $(PDFLATEXFLAGS) $(basename $<)
	$(PDFLATEX) $(PDFLATEXFLAGS) $(basename $<)


clean::
	$(RM) *.aux *.bbl *.blg *.dvi *.log *.out

realclean:: clean
	$(RM) *.pdf
